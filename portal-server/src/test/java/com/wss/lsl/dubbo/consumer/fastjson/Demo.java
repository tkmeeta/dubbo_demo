package com.wss.lsl.dubbo.consumer.fastjson;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * TODO: 增加描述
 * 
 * @author wei.ss
 * @date 2015-12-30 上午9:45:33
 * @version 1.0.0 
 */
public class Demo {
	
	private String id;
	
	String name;
	
	@JSONField(name="demo2List", serialize=true, serialzeFeatures={SerializerFeature.BeanToArray})
	private List<Demo2> list = new ArrayList<Demo2>();
	private Demo2 demo2;
	
	public Demo() {
		list.add(new Demo2());
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return "xxxxx";
	}

	public void setName(String name) {
		this.name = name;
	}
		
	public List<Demo2> getList() {
		return list;
	}

	public void setList(List<Demo2> list) {
		this.list = list;
	}

	public Demo2 getDemo2() {
		return list.get(0);
	}

	public void setDemo2(Demo2 demo2) {
		this.demo2 = demo2;
	}

	public static void main(String[] args) {
		Demo demo = new Demo();
		List<Demo2> list = demo.getList();
		
		System.out.println(JSON.toJSONString(demo.demo2));
		System.out.println(JSON.toJSONString(list));
		
		String json = JSON.toJSONString(demo);
		System.out.println(json);
		
		Demo demo2 = (Demo)JSON.parseObject(json, Demo.class);
		List<Demo2> list2 = demo2.getList();
		System.out.println(JSON.toJSONString(list2));
	}
}
