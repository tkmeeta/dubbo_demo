package com.wss.lsl.dubbo.consumer.fastjson;

import java.io.Serializable;

/**
 * TODO: 增加描述
 * 
 * @author wei.ss
 * @date 2015-12-30 上午9:47:50
 * @version 1.0.0 
 * @copyright wonhigh.net.cn 
 */
public class Demo2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String name;
	private Demo3 demo3;

	public Demo2() {
		this.demo3 = new Demo3();
	}

	public Demo2(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return "yyyyyy";
	}

	public void setName(String name) {
		this.name = name;
	}

	public Demo3 getDemo3() {
		return demo3;
	}

	public void setDemo3(Demo3 demo3) {
		this.demo3 = demo3;
	}
	
}
