package com.wss.lsl.dubbo.consumer.fastjson;

public class Demo3 {

	private String name;

	public Demo3() {
		this.name = "demo3";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
