<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录页面</title>
</head>
<body>
这是登录页面<br />

<form action="<%=contextPath %>/login/do_login.do" method="post">
	<table>
		<tr>
			<td>用户名</td>
			<td>
				<input type="text" name="username" />
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="submit" value="登录" />
			</td>
		</tr>
	</table>
</form>
</body>
</html>