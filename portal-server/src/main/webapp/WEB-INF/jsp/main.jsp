<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>会员系统</title>
</head>
<body>
当前登录用户：${loginUser.username }<a href="<%=contextPath%>/login/do_logout.do" style="margin-left: 30px;">注销</a>
<br />
<a href="/elt/main/index.do?currentUser=${currentUser }" target="_self" style="margin-top: 50px;">跨应用session共享</a>
</body>
</html>