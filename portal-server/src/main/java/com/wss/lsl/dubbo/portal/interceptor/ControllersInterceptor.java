package com.wss.lsl.dubbo.portal.interceptor;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.wss.lsl.server.util.Permission;
import com.wss.lsl.server.util.dto.LoginUser;

public class ControllersInterceptor extends HandlerInterceptorAdapter {
	
	private static final Logger LOG = LoggerFactory.getLogger(ControllersInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		HttpSession session = request.getSession();
		LoginUser loginUser = (LoginUser)session.getAttribute("loginUser");
		
		HandlerMethod handlerMethod = (HandlerMethod)handler;
		Method requestMethod = handlerMethod.getMethod();
		Permission permission = requestMethod.getAnnotation(Permission.class);
		
		if(null == permission || permission.needLogin()) {
			if(null == loginUser) {
				LOG.info("没有登录，请重新登录");
				
				response.sendRedirect(request.getContextPath() + "/");
				return false;
			}
		}
		
		return true;
	}
	
	
}
