package com.wss.lsl.dubbo.portal.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.wss.lsl.dubbo.demo.model.Member;
import com.wss.lsl.dubbo.demo.server.MemberService;
import com.wss.lsl.server.util.ServerConstant;

/**
 * 会员 controller
 * 
 * @author wei.ss
 */
@Controller
@RequestMapping("member")
public class MemberController {

	private static final Logger LOG = LoggerFactory.getLogger(MemberController.class);
	
	@Autowired
	private MemberService memberService;
	
	@RequestMapping(value="/addMember")
	public String addMember(@ModelAttribute Member member){
		
		LOG.info("准备添加会员");
		memberService.saveMember(member);
		LOG.info("添加会员完毕");
		
		return "member/member_list";
	}
	
	@RequestMapping(value="/getMemberById", produces={ServerConstant.APPLICATION_JSON_UTF_8})
	@ResponseBody
	public Member getMemberById(String id){
		
		LOG.info("查询会员参数: id={}", id);
		
		Member member = memberService.getMemberById(id);
		
		LOG.info("查询会员结果: member={}", JSON.toJSONString(member));
		return member;
	}
}
