package com.wss.lsl.dubbo.portal.interceptor;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.wss.lsl.server.util.ServerConstant;
import com.wss.lsl.server.util.dto.LoginUser;

import redis.clients.jedis.JedisPool;

/**
 * session interceptor
 * 
 * @author sean
 */
public class SessionInterceptor extends com.wss.lsl.server.util.SessionInterceptor {
	@Autowired
	private JedisPool redisPool;

	@Override
	public JedisPool getJedisPool() {
		return redisPool;
	}

	@Override
	public String getSharedKey(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		LoginUser userLoginDTO = (LoginUser) session.getAttribute("loginUser");
		if (null != userLoginDTO) {
			return userLoginDTO.getUsername() + "_" + userLoginDTO.getLoginTime();
		}

		String currentUser = request.getParameter(ServerConstant.CURRENT_USER);
		if (null != currentUser) {
			return currentUser;
		}

		return (String) session.getAttribute("logoutUser");
	}

	@Override
	public boolean isClearSession(HttpServletRequest request) {
		String currentUrl = request.getServletPath();
		if (null != currentUrl) {
			currentUrl = currentUrl.toLowerCase(Locale.getDefault());
		}

		return "/login/do_logout.do".equals(currentUrl);
	}

	@Override
	public boolean isCreateWhenSessionNotfound() {
		return true;
	}
}
