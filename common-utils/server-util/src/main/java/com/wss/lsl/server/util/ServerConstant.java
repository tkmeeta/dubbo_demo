package com.wss.lsl.server.util;

import org.springframework.http.MediaType;

/**
 * 服务名常量
 * 
 * @author sean
 */
public final class ServerConstant {

	/**
	 * UTF-8编码的json格式
	 */
	public static final String APPLICATION_JSON_UTF_8 = MediaType.APPLICATION_JSON_VALUE + "; charset=UTF-8";

	/**
	 * portal-server服务名
	 */
	public static final String PORTAL_SERVER_NAME = "portal";

	public static final String CURRENT_USER = "currentUser";
	
	/**
	 * portal登出地址
	 */
	public static final String PORTAL_LOGOUT_URL = "/login/do_logout.do";
}