package com.wss.lsl.dubbo.demo.tools.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {
	
	private static final Logger LOG = LoggerFactory.getLogger(DateUtil.class);
	
	/**
	 * 日期格式: 20151212121212
	 */
	public static final String DATE_FORMAT_YYYYMMDDHHMISS = "YYYYMMDDHHmmss";
	/**
	 * 日期格式: 2015-12-12 12:12:12
	 */
	public static final String DATE_FORMAT_YYYY_MM_DD_HH_MI_SS = "YYYY-MM-DD HH:mm:ss";
	
	public static String date2String(Date date, String format){
		
		LOG.info("日期转化字符串参数：date={}, format={}", date, format);
		
		if(null == date) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	
	public static Date string2Date(String str, String format){
		
		LOG.info("字符串转化日期参数: str={}, format={}", str, format);
		if(null == str) {
			return null;
		}
		
		DateFormat dateFormat = new SimpleDateFormat(format);
		try {
			return dateFormat.parse(str);
		} catch (ParseException e) {
			LOG.error("字符串转化为日期异常", e);
			
			return null;
		}
		
	}
}
