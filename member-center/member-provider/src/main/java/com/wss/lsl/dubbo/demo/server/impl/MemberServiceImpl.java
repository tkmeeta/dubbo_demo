package com.wss.lsl.dubbo.demo.server.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.wss.lsl.dubbo.demo.model.Member;
import com.wss.lsl.dubbo.demo.server.MemberService;

@Service("memberService")
public class MemberServiceImpl implements MemberService {
	
	private static final Logger LOG = LoggerFactory.getLogger(MemberServiceImpl.class);
	
	private static final Map<String, Member> DATA_MAP = new HashMap<String, Member>();

	@Override
	public void saveMember(Member member) {
		String id = member.getId();
		DATA_MAP.put(id, member);
		
		long time = System.currentTimeMillis() / 1000;
		if(time % 3 == 0) {
			getMemberById(id);
		}
	}

	@Override
	public Member getMemberById(String id) {
		LOG.info("获取会员: id={}", id);
		return DATA_MAP.get(id);
	}

}
