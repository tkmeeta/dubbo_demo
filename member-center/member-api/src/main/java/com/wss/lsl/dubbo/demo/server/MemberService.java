package com.wss.lsl.dubbo.demo.server;

import com.wss.lsl.dubbo.demo.model.Member;

/**
 * 会员服务
 * 
 * @author wei.ss
 *
 */
public interface MemberService {
	
	void saveMember(Member member);
	
	Member getMemberById(String id);
}
