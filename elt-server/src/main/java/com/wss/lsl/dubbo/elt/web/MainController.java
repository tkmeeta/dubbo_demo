package com.wss.lsl.dubbo.elt.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * main controller
 * 
 * @author sean
 *
 */
@Controller
@RequestMapping(value="/main")
public class MainController {
	
	private static final Logger LOG = LoggerFactory.getLogger(MainController.class);
	
	@RequestMapping(value="/index")
	public String index(){
		LOG.info("进入elt主页面");
		
		return "main";
	}
}
