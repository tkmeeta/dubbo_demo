package com.wss.lsl.dubbo.elt.interceptor;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wss.lsl.server.util.ServerConstant;
import com.wss.lsl.server.util.dto.LoginUser;

import redis.clients.jedis.JedisPool;

/**
 * session interceptor
 * 
 * @author sean
 */
public class SessionInterceptor extends com.wss.lsl.server.util.SessionInterceptor {
	
	private static final Logger LOG = LoggerFactory.getLogger(SessionInterceptor.class);
	@Autowired
	private JedisPool redisPool;

	@Override
	public JedisPool getJedisPool() {
		return redisPool;
	}

	@Override
	public String getSharedKey(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		LoginUser userLoginDTO = (LoginUser) session.getAttribute("loginUser");
		if (null != userLoginDTO) {
			return userLoginDTO.getUsername() + "_" + userLoginDTO.getLoginTime();
		}

		String currentUser = request.getParameter(ServerConstant.CURRENT_USER);
		if (null != currentUser) {
			return currentUser;
		}

		return null;
	}

	@Override
	public boolean isClearSession(HttpServletRequest request) {
		
		return false;
	}

	@Override
	public boolean isCreateWhenSessionNotfound() {
		return false;
	}
}
