###dubbo版本2.5.3

```xml
<dependency>
	<groupId>com.alibaba</groupId>
	<artifactId>dubbo</artifactId>
	<version>2.5.3</version>
	<exclusions>
		<exclusion>
			<artifactId>spring</artifactId>
			<groupId>org.springframework</groupId>
		</exclusion>
	</exclusions>
</dependency>
```

监控中心，在提供者、消费者端配置，主要用于各指标的统计。

```xml
<!-- 通过注册中心发现监控中心 -->
<dubbo:monitor protocol="registry" />
```

####dubbo配置文件见src/main/resources/dubbo目录

* 提供者项目：member-provider

* 消费者项目：portal-server

* 共同依赖的接口项目：member-api。提供者和消费者共享的接口项目

###直连方式，不需要注册中心

* 提供者配置

```xml
<dubbo:application name="member-provider" owner="wei.ss" />
<dubbo:protocol name="dubbo" port="20880" />
<!-- 不使用注册中心 
<dubbo:provider registry="N/A" />
或者如下register="false"
-->
<dubbo:registry address="127.0.0.1" register="false" />

<dubbo:service ref="memberService"
	interface="com.wss.lsl.dubbo.demo.server.MemberService" />
```

* 消费者配置

```xml
<dubbo:application name="portal-server" owner="wei.ss" />			
<dubbo:reference id="memberService" url="dubbo://127.0.0.1:20880"
	interface="com.wss.lsl.dubbo.demo.server.MemberService"/>
```

###zookeeper注册中心

zookeeper client maven依赖

```xml
<dependency>
	<groupId>com.github.sgroschupf</groupId>
	<artifactId>zkclient</artifactId>
	<version>0.1</version>
</dependency>
```

* 提供者配置

```xml
<dubbo:application name="member-provider" owner="wei.ss" />
<dubbo:protocol name="dubbo" port="20880" />

<!-- zookeeper -->
<dubbo:registry protocol="zookeeper" address="127.0.0.1:2181" />

<dubbo:service ref="memberService"
	interface="com.wss.lsl.dubbo.demo.server.MemberService" />
```

* 消费者配置

```xml
<dubbo:application name="portal-server" owner="wei.ss" />
<dubbo:registry protocol="zookeeper" address="127.0.0.1:2181" />
		
<dubbo:reference id="memberService"
	interface="com.wss.lsl.dubbo.demo.server.MemberService"/>
```

###redis注册中心

redis client maven依赖

```xml
<dependency>
	<groupId>redis.clients</groupId>
	<artifactId>jedis</artifactId>
	<version>2.0.0</version>
</dependency>
<dependency>
	<groupId>commons-pool</groupId>
	<artifactId>commons-pool</artifactId>
	<version>1.6</version>
</dependency>
```

* 提供者配置

```xml
<dubbo:application name="member-provider" owner="wei.ss" />
<dubbo:protocol name="dubbo" port="20880" />
<!-- redis -->
<dubbo:registry protocol="redis" address="172.17.210.80:6379" />

<dubbo:service ref="memberService"
	interface="com.wss.lsl.dubbo.demo.server.MemberService" />
```

* 消费者配置

```xml
<dubbo:application name="portal-server" owner="wei.ss" />
<!-- redis -->
<dubbo:registry protocol="redis" address="172.17.210.80:6379" />
			
<dubbo:reference id="memberService"
	interface="com.wss.lsl.dubbo.demo.server.MemberService"/>
```
